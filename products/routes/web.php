<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/* List and details of products */
Route::get('products', 'ProductsController@index');
Route::get('products/readCSV', 'ProductsController@readCSV');
Route::get('products/{product}', 'ProductsController@show');

/* Route to read the csv file */
