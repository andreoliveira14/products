<?php
// Author: André Oliveira

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Product;
use App\Brand;
use App\Location;
use App\Image;
use Log;

class ProductsController extends Controller
{
    /*
     * Function that shows all the products
     */
    public function index()
    {
        // get all the products
        $products = Product::all();


        $brandAndCount = [];
        $brands = Brand::all();
        // get all the brands and products counts
        foreach($brands as $brand) {
            if(array_key_exists($brand->name, $brandAndCount)) {
                $brandAndCount[$brand->name] = $brandAndCount[$brand->name] + 1;
            } else {
                $brandAndCount[$brand->name] = 1;
            }
        }

        $viewData = [
            'products' => $products,
            'brands'   => $brandAndCount
        ];

        return view('products.index', $viewData);
    }

    /*
     * Show the details of one product
     */
    public function show(Product $product) {
        return view('products.show', ['product' => $product]);
    }

    /*
     * Read the csv file with all the products from storage path
     */
    public function readCSV()
    {
        //get file path
        $path = storage_path('Products.csv');
        $file = file($path);

        $products = [];
        foreach ($file as $line) {
            $products[] = str_getcsv($line);
        }

        //remove first position of the array with the titles
        unset($products[0]);

        foreach ($products as $product) {
            $this->createproduct($product);
        }

        return 'The CSV was successfully imported!';
    }

    /*
     * Create new product
     *
     * $product[0] => name
     * $product[1] => brand
     * $product[2] => sku
     * $product[3] => description
     * $product[4] => price
     * $product[5] => rrp
     * $product[6] => active
     * $product[7] => locations
     * $product[8] => imageUrls
     */
    private function createProduct($product)
    {
        // get the product information to create
        $productToCreate = [
            'name'        => $product[0],
            'sku'         => $product[2],
            'description' => $product[3],
            'price'       => $product[4],
            'rrp'         => $product[5],
            'active'      => $product[6],
        ];
        $finalProduct = Product::firstOrCreate($productToCreate);

        $this->createBrand($finalProduct, $product[1]);
        $this->createLocations($finalProduct, $product[7]);
        $this->createImages($finalProduct, $product[8]);

    }

    /*
     * Create Brands
     */
    private function createBrand($finalProduct, $brandName)
    {
        $brandToCreate = [
            'name'       => $brandName,
            'product_id' => $finalProduct->id
        ];
        Brand::firstOrCreate($brandToCreate);
    }

    /*
     * Create Locations
     */
    private function createLocations($finalProduct, $productLocations)
    {
        // locations
        $locations = explode('||', $productLocations);
        if(!empty($locations[0])) {
            foreach($locations as $location) {
                $locationAndTotal = explode(':', $location);
                $imageToCreate = [
                    'name'       => $locationAndTotal[0],
                    'total'      => $locationAndTotal[1],
                    'product_id' => $finalProduct->id
                ];
                Location::firstOrCreate($imageToCreate);
            }
        }
    }

    /*
     * Create Images
     */
    private function createImages($finalProduct, $productImageUrls)
    {
        // images
        $imagesUrls = explode('||', $productImageUrls);
        if(!empty($imagesUrls[0])) {
            foreach($imagesUrls as $imageUrl) {
                $imageToCreate = [
                    'url'        => $imageUrl,
                    'product_id' => $finalProduct->id
                ];
                Image::firstOrCreate($imageToCreate);
            }
        }
    }

}
