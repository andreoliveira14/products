<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['name', 'total', 'product_id'];

    /**
     * Get the product that owns the location.
     */
    public function post()
    {
        return $this->belongsTo('App\Product');
    }
}
