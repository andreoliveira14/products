<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = ['name', 'sku', 'description', 'price', 'rrp', 'active'];

    /*
     * 1 Product -> * Images
     */
    public function images()
    {
        return $this->hasMany('App\Image');
    }

    /*
     * 1 Product -> * Locations
     */
    public function locations()
    {
        return $this->hasMany('App\Location');
    }

    /*
     * 1 Product -> 1 Brand
     */
    public function brand()
    {
        return $this->hasOne('App\Brand');
    }
}
