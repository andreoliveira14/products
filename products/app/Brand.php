<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'product_id'];

    /**
     * Get the product that owns the brand.
     */
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}
