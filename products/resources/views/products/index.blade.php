<!DOCTYPE html>
<html>
    <head>
        <title>Products</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">
                    <h3>Brands List</h3>
                </div>
                <hr>
                <table>
                    <theader>
                        <tr>
                            <th> Brand </th>
                            <th> Total </th>
                        </tr>
                    </theader>

                    <tbody>
                        @foreach($brands as $name => $productCounts)
                            <tr>
                                <td>
                                    {{ $name }}
                                </td>
                                <td>
                                    {{ $productCounts }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>

            <hr>

            <div class="content">
                <div class="title">
                    <h3>Products List</h3>
                </div>
                <hr>
                <ul>
                    @foreach($products as $product)
                        <li>
                            <a href="{{ url('/products/' . $product->id) }}"> {{ $product->name }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </body>
</html>
