<!DOCTYPE html>
<html>
    <head>
        <title>Product - {{ $product->name }}</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="container">
            <div class="content">

                <h2>{{ $product->name }}</h2>
                <hr>
                <strong>Description</strong> : {{ $product->description }}
                <br>
                <strong>Brand</strong> : {{ $product->brand->name }}
                <br>
                <strong>Price</strong> : {{ $product->price }}
                <br>
                <strong>rrp</strong> : {{ $product->rrp }}
                <br>
                <strong>Active</strong> :
                @if($product->images->isEmpty())
                    No
                @else
                    {{ $product->active }}
                @endif
                <br>
                <strong>Locations</strong> :
                <ul>
                    @foreach($product->locations as $location)
                        <li>{{ $location->name }} : {{ $location->total }}</li>
                    @endforeach
                </ul>
                <strong>Images</strong> :
                @if($product->images->isEmpty())
                    -
                @else
                    <ul>
                        @foreach($product->images as $image)
                            <li>{{ $image->url }}</li>
                        @endforeach
                    </ul>
                @endif

                <hr>

                <a href="{{ url('/products') }}"> back</a>
            </div>
        </div>
    </body>
</html>
